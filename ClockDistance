# ------- Script to calculate distance between two hands of a clock ---------
# ------- args: --mLength length of minute hand -----------------------------
# -------       --hLength length of hour hand -------------------------------
# -------       --time time at which to find distance (HH:MM) ---------------

import sys
import getopt
import math
import numpy
import time


def find_hour_angle(hour, minute):
    hour_contribution = hour * ((2 * math.pi) / 12)
    minute_contribution = minute * (((2 * math.pi) / 12) / 60)

    return hour_contribution + minute_contribution


def find_minute_angle(minute):
    minute_contribution = minute * ((2 * math.pi) / 60)

    return minute_contribution


def find_hand_vector(angle, len):
    # angle is from positive y-axis (12:00), for vector calcs we need from pos x-axis (3:00)
    math_angle = (math.pi / 2) - angle

    return len * numpy.array([math.cos(math_angle), math.sin(math_angle)])


hlength = 0
mlength = 0
t = time.localtime(time.time())

try:
    opts, args = getopt.getopt(sys.argv[1:], "m:h:t:", ["mlength=", "hlength=", "time="])
except getopt.GetoptError:
    print('test.py -m <mLength> -h <hLength> -t <time>')
    sys.exit(2)
for (opt, arg) in opts:
    if opt in ("-h", "--hlength"):
        hlength = float(arg)
    elif opt in ("-m", "--mlength"):
        mlength = float(arg)
    elif opt in ("-t", "--time"):
        t = time.strptime(arg, "%H:%M")

hour = t.tm_hour
minute = t.tm_min

minute_vector = find_hand_vector(find_minute_angle(minute), mlength)
hour_vector = find_hand_vector(find_hour_angle(hour, minute), hlength)
difference = minute_vector - hour_vector

print(numpy.linalg.norm(difference))
